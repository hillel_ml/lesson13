import python_speech_features 
import numpy as np
import pandas as pd
import tensorflow as tf
import matplotlib.pyplot as plt
import os
import wave
import pylab
from pathlib import Path
from scipy import signal
from scipy.io import wavfile
import scipy.io.wavfile as wav
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
import itertools
import seaborn as sns

import keras
from keras import losses, models, optimizers
from keras.activations import relu, softmax
from keras.callbacks import (EarlyStopping, LearningRateScheduler,
                             ModelCheckpoint, TensorBoard, ReduceLROnPlateau)
from keras.layers import Dense, Conv1D, Flatten, MaxPooling1D, Activation
from keras.utils import Sequence, to_categorical

import librosa
# from python_speech_features import mfcc


# Set paths to input and output data
INPUT_DIR = 'lesson13/spoken_digit_dataset/recordings/'
OUTPUT_DIR = 'lesson13/output/'

# Utility function to get sound and frame rate info
def get_wav_info(wav_file):
    wav = wave.open(wav_file, 'r')
    frames = wav.readframes(-1)
    sound_info = pylab.frombuffer(frames, 'int16')
    frame_rate = wav.getframerate()
    wav.close()
    return sound_info, frame_rate

# For every recording, make a spectogram and save it as label_speaker_no.png
if not os.path.exists(os.path.join(OUTPUT_DIR, 'audio-images')):
    os.mkdir(os.path.join(OUTPUT_DIR, 'audio-images'))

# files = [f for f in os.listdir(INPUT_DIR)] 
# wav, _ = librosa.core.load(os.path.join(INPUT_DIR,files[0]), sr=8000)
# mfcc = python_speech_features.mfcc(wav,8000,numcep=26)
# # # mfcc = mfcc(wav,16000)
# print(mfcc.shape)
# plt.axis('off')
# plt.imshow(mfcc, cmap='hot', interpolation='nearest')
# print(mfcc.shape)

for filename in os.listdir(INPUT_DIR):
    if "wav" in filename:
        file_path = os.path.join(INPUT_DIR, filename)
        file_stem = Path(file_path).stem
        target_dir = f'class_{file_stem[0]}'
        dist_dir = os.path.join(os.path.join(OUTPUT_DIR, 'audio-images'), target_dir)
        file_dist_path = os.path.join(dist_dir, file_stem)
        if not os.path.exists(file_dist_path + '.png'):
            if not os.path.exists(dist_dir):
                os.mkdir(dist_dir)
            file_stem = Path(file_path).stem
            sound_info, frame_rate = get_wav_info(file_path)
            wav, _ = librosa.core.load(file_path, sr=8000)
            plt.figure()
            plt.axis('off')
#             sound = sound_info.reshape(1,-1)
#             mfcc = librosa.feature.mfcc(wav, sr = 8000, n_mfcc=40)
            mfcc = python_speech_features.mfcc(wav,8000)
            plt.imshow(mfcc, cmap='hot', interpolation='nearest')
#             plt.specgram(sound_info, NFFT=1024, Fs=frame_rate, noverlap=900)
            plt.savefig(f'{file_dist_path}.png',bbox_inches="tight", pad_inches=0)
            plt.close()
#             pylab.specgram(sound_info, Fs=frame_rate)
#             pylab.savefig(f'{file_dist_path}.png',bbox_inches="tight", pad_inches=0)
#             pylab.close()
#             break

# Print the ten classes in our dataset
path_list = os.listdir(os.path.join(OUTPUT_DIR, 'audio-images'))
print("Classes: \n")
for i in range(1):
    print(path_list[i])
    
# File names for class 1
path_list = os.listdir(os.path.join(OUTPUT_DIR, 'audio-images/class_1'))
print("\nA few example files: \n")
for i in range(1):
    print(path_list[i])



############################

# Declare constants
IMAGE_HEIGHT = 40
IMAGE_WIDTH = 40
BATCH_SIZE = 32
N_CHANNELS = 3
N_CLASSES = 10

# Make a dataset containing the training spectrograms
train_dataset = tf.keras.preprocessing.image_dataset_from_directory(
                                             batch_size=BATCH_SIZE,
                                             validation_split=0.2,
                                             directory=os.path.join(OUTPUT_DIR, 'audio-images'),
                                             shuffle=True,
                                             color_mode='rgb',
                                             image_size=(IMAGE_HEIGHT, IMAGE_WIDTH),
                                             subset="training",
                                             seed=0)

# Make a dataset containing the validation spectrogram
valid_dataset = tf.keras.preprocessing.image_dataset_from_directory(
                                             batch_size=BATCH_SIZE,
                                             validation_split=0.2,
                                             directory=os.path.join(OUTPUT_DIR, 'audio-images'),
                                             shuffle=True,
                                             color_mode='rgb',
                                             image_size=(IMAGE_HEIGHT, IMAGE_WIDTH),
                                             subset="validation",
                                             seed=0)

label_names = np.array(train_dataset.class_names)
print()
print("label names:", label_names)





# Function to prepare our datasets for modelling
def prepare(ds, augment=False):
    # Define our one transformation
    rescale = tf.keras.Sequential([tf.keras.layers.experimental.preprocessing.Rescaling(1./255)])
    flip_and_rotate = tf.keras.Sequential([
        tf.keras.layers.experimental.preprocessing.RandomFlip("horizontal_and_vertical"),
        tf.keras.layers.experimental.preprocessing.RandomRotation(0.2)
    ])
    
    # Apply rescale to both datasets and augmentation only to training
    ds = ds.map(lambda x, y: (rescale(x, training=True), y))
    if augment: ds = ds.map(lambda x, y: (flip_and_rotate(x, training=True), y))
    return ds

train_dataset = prepare(train_dataset, augment=False)
valid_dataset = prepare(valid_dataset, augment=False)
for images, labels in train_dataset.take(1):
    for i in range(9):
        ax = plt.subplot(3, 3, i + 1)
        plt.imshow(images[i].numpy())
        plt.title(int(labels[i]))
        plt.axis("off")
plt.show()


############################

# Create CNN model
model = tf.keras.models.Sequential()
model.add(tf.keras.layers.Input(shape=(IMAGE_HEIGHT, IMAGE_WIDTH, N_CHANNELS)))
model.add(tf.keras.layers.Conv2D(32, 3, strides=2, padding='same', activation='relu'))
model.add(tf.keras.layers.BatchNormalization())
model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))
model.add(tf.keras.layers.BatchNormalization())
model.add(tf.keras.layers.Conv2D(64, 3, padding='same', activation='relu'))
model.add(tf.keras.layers.BatchNormalization())
model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))
model.add(tf.keras.layers.BatchNormalization())
model.add(tf.keras.layers.Conv2D(128, 3, padding='same', activation='relu'))
model.add(tf.keras.layers.BatchNormalization())
model.add(tf.keras.layers.MaxPooling2D(pool_size=(2, 2)))
model.add(tf.keras.layers.BatchNormalization())
model.add(tf.keras.layers.Flatten())
model.add(tf.keras.layers.Dense(256, activation='relu'))
model.add(tf.keras.layers.BatchNormalization())
model.add(tf.keras.layers.Dropout(0.5))
model.add(tf.keras.layers.Dense(N_CLASSES, activation='softmax'))

model.summary()

model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath='lesson13/checkpoints/checkpoint.h5',
    save_weights_only=True,
    monitor='val_accuracy',
    mode='max',
    save_best_only=True)

# Compile model
model.compile(
    loss='sparse_categorical_crossentropy',
    optimizer=tf.keras.optimizers.RMSprop(),
    metrics=['accuracy'],
)

# Train model for 10 epochs, capture the history
history = model.fit(train_dataset,
                    epochs=20,
                    callbacks=model_checkpoint_callback,
                    validation_data=valid_dataset
                    )


# Plot the loss curves for training and validation.
history_dict = history.history
loss_values = history_dict['loss']
val_loss_values = history_dict['val_loss']
epochs = range(1, len(loss_values)+1)

plt.figure()
plt.plot(epochs, loss_values, 'bo', label='Training loss')
plt.plot(epochs, val_loss_values, 'b', label='Validation loss')
plt.title('Training and validation loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()
plt.show()

# Plot the accuracy curves for training and validation.
acc_values = history_dict['accuracy']
val_acc_values = history_dict['val_accuracy']
epochs = range(1, len(acc_values)+1)

plt.figure()
plt.plot(epochs, acc_values, 'bo', label='Training accuracy')
plt.plot(epochs, val_acc_values, 'b', label='Validation accuracy')
plt.title('Training and validation accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.legend()
plt.show()

# Compute the final loss and accuracy
final_loss, final_acc = model.evaluate(valid_dataset, verbose=0)
print("Final loss: {0:.6f}, final accuracy: {1:.6f}".format(final_loss, final_acc))

# # Clean the output dir
# import shutil
# shutil.rmtree('/kaggle/working/audio-images')

y_pred = model.predict(valid_dataset)
y_pred = tf.argmax(y_pred, axis=1)
y_true = tf.concat(list(valid_dataset.map(lambda s,lab: lab)), axis=0)


confusion_mtx = tf.math.confusion_matrix(y_true, y_pred)
plt.figure(figsize=(10, 8))
sns.heatmap(confusion_mtx,
            xticklabels=label_names,
            yticklabels=label_names,
            annot=True, fmt='g')
plt.xlabel('Prediction')
plt.ylabel('Label')
plt.savefig('lesson13/output/confusion_mtx.png')
plt.show()


tf.keras.utils.plot_model(
    model,
    to_file="lesson13/output/model.png",
    show_shapes=False,
    show_dtype=False,
    show_layer_names=True,
    rankdir="TB",
    expand_nested=False,
    dpi=96,
)


# class ExportModel(tf.Module):
#     def __init__(self, model):
#         self.model = model

#         # Accept either a string-filename or a batch of waveforms.
#         # YOu could add additional signatures for a single wave, or a ragged-batch. 
#         self.__call__.get_concrete_function(
#             x=tf.TensorSpec(shape=(), dtype=tf.string))
#         self.__call__.get_concrete_function(
#         x=tf.TensorSpec(shape=[None, 16000], dtype=tf.float32))


#     @tf.function
#     def __call__(self, x):
#         # If they pass a string, load the file and decode it.
#         if x.dtype == tf.string:
#             x = tf.io.read_file(x)
#             x, _ = tf.audio.decode_wav(x, desired_channels=1, desired_samples=16000,)
#             x = tf.squeeze(x, axis=-1)
#             x = x[tf.newaxis, :]

#         x = get_spectrogram(x)
#         result = self.model(x, training=False)

#         class_ids = tf.argmax(result, axis=-1)
#         class_names = tf.gather(label_names, class_ids)
#         return {'predictions':result,
#                 'class_ids': class_ids,
#                 'class_names': class_names}


# export = ExportModel(model)
# export(tf.constant(str(data_dir/'no/01bb6a2a_nohash_0.wav')))

# tf.saved_model.save(export, "lesson13/saved")
# imported = tf.saved_model.load("lesson13/saved")
# imported(waveform[tf.newaxis, :])