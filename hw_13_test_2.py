import matplotlib.pyplot as plt
import numpy as np
import wave
import sys
import librosa
import librosa.display
import pandas as pd
import scipy
import seaborn as sns
import os, random



input_dir = "lesson13/spoken_digit_dataset/recordings/"
rand_file = random.choice(os.listdir("lesson13/spoken_digit_dataset/recordings/")) #change dir name to whatever

input_file = os.path.join(input_dir, rand_file)


wav_obj = wave.open(input_file, "rb")

print(type(wav_obj))

sample_freq = wav_obj.getframerate()
print(sample_freq)


n_samples = wav_obj.getnframes()
print(n_samples)


t_audio = n_samples/sample_freq
print(t_audio)

n_channels = wav_obj.getnchannels()
print(n_channels)


signal_wave = wav_obj.readframes(n_samples)

signal_array = np.frombuffer(signal_wave, dtype=np.int16)

l_channel = signal_array[0::n_channels]
r_channel = signal_array[1::n_channels]

times = np.linspace(0, n_samples/sample_freq, num=n_samples)


y, sr = librosa.load(input_file)

y_harmonic, y_percussive = librosa.effects.hpss(y)



plt.figure(figsize=(15, 5))
plt.plot(times, l_channel)
plt.title('Waveform of random wav file')
plt.ylabel('Frequency (Hz)')
plt.xlabel('Time (s)')
plt.show()


plt.figure(figsize=(15, 5))
librosa.display.waveshow(y_harmonic, sr=sr, alpha=0.25)
librosa.display.waveshow(y_percussive, sr=sr, color='r', alpha=0.5)
plt.title('Harmonic + Percussive')
plt.ylabel('Frequency (Hz)')
plt.xlabel('Time (s)')
plt.show()


plt.figure(figsize=(15, 5))
plt.specgram(l_channel, Fs=sample_freq)
plt.title('Spectrogram (matplotlib)')
plt.ylabel('Frequency (Hz)')
plt.xlabel('Time (s)')
plt.colorbar()
plt.show()


X = librosa.stft(y)
Xdb = librosa.amplitude_to_db(abs(X))
plt.figure(figsize=(15, 5))
librosa.display.specshow(Xdb, sr=sr, x_axis='time', y_axis='log')
plt.title('Spectrogram (librosa)')
plt.ylabel('Frequency (Hz)')
plt.xlabel('Time (s)')
plt.colorbar()
plt.show()


# Calculate MFCCs
mfccs = librosa.feature.mfcc(y=y_harmonic, sr=sr, n_mfcc=20)
plt.figure(figsize=(15, 5))
librosa.display.specshow(mfccs, x_axis='time', y_axis='log')
plt.title('MFCC')
plt.ylabel('Frequency (Hz)')
plt.xlabel('Time (s)')
plt.colorbar()
plt.show()


chroma = librosa.feature.chroma_cens(y=y_harmonic, sr=sr)
plt.figure(figsize=(15, 5))
librosa.display.specshow(chroma,y_axis='chroma', x_axis='time')
plt.title('Chroma Energy Normalized (CENS)')
plt.xlabel('Time (s)')
plt.colorbar()
plt.show()


fig, axes = plt.subplots(5, 
                        figsize=(15, 10),
                        # sharex=True
                        )
# plt.suptitle('Vertically stacked subplots')
axes[0].plot(times, l_channel)
axes[0].set_title('Waveform')
axes[0].set_xlim(0, t_audio)

librosa.display.waveshow(y_harmonic, sr=sr, alpha=0.25, ax=axes[1])
librosa.display.waveshow(y_percussive, sr=sr, color='r', alpha=0.5, ax=axes[1])
axes[1].set_title('Harmonic + Percussive')
axes[1].set_xlim(0, t_audio)

axes[2].specgram(l_channel, Fs=sample_freq)
axes[2].set_title('Spectrogram')

librosa.display.specshow(mfccs, x_axis='time', y_axis='log', ax=axes[3])
axes[3].set_title('MFCC')
axes[3].label_outer()

librosa.display.specshow(chroma,y_axis='chroma', x_axis='time', ax=axes[4])
axes[4].set_title('Chroma Energy Normalized (CENS)')
plt.show()
