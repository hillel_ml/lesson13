import thinkdsp
import thinkplot
import matplotlib.pyplot as plt

import numpy as np

cos_sig = thinkdsp.CosSignal(freq=440, amp=1.0, offset=0)
sin_sig = thinkdsp.SinSignal(freq=880, amp=0.5, offset=0)

cos_sig.plot()
thinkplot.config(xlabel='Time (s)')
plt.show()

sin_sig.plot()
thinkplot.config(xlabel='Time (s)')
plt.show()

mix = sin_sig + cos_sig
mix.plot()
plt.show()

wave = mix.make_wave(duration=0.5, start=0, framerate=11025)
wave.make_audio()

wave.normalize()
wave.apodize()
wave.plot()
thinkplot.config(xlabel='Time (s)')
wave.write('temp.wav')


