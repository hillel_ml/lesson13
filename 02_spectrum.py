import thinkdsp
import thinkplot
import matplotlib.pyplot as plt

import numpy as np

wave = thinkdsp.read_wave('violin-origional.wav')
wave.make_audio()

start = 1.2
duration = 0.6
segment = wave.segment(start, duration)
segment.plot()
thinkplot.config(xlabel='Time (s)')
plt.show()

spectrum = segment.make_spectrum()
spectrum.plot()
thinkplot.config(xlabel='Frequency (Hz)')
plt.show()

spectrum.plot(high=10000)
thinkplot.config(xlabel='Frequency (Hz)')
plt.show()


