import thinkdsp
import thinkplot

import random
import matplotlib.pyplot as plt
from functools import reduce


def midi_to_freq(midi_num):
    """Converts MIDI note number to frequency.

    midi_num: int MIDI note number

    returns: float frequency in Hz
    """
    x = (midi_num - 69) / 12.0
    freq = 440.0 * 2 ** x
    return freq


def random_freq(midi_num):
    # simulate poor tuning by adding gaussian noise to the MIDI number
    midi_num += random.gauss(0, 0.5)

    # one kid out of 10 plays the wrong note
    if random.random() < 0.1:
        midi_num += random.randint(-5, 5)

    freq = midi_to_freq(midi_num)

    # and one kid in 10 pops an overtone
    if random.random() < 0.1:
        freq *= random.randint(2, 5)

    return freq

def make_note(midi_num, duration, framerate=22050):
    """Make a MIDI note with the given duration.

    midi_num: int MIDI note number
    duration: float seconds
    sig_cons: Signal constructor function
    framerate: int frames per second

    returns: Wave
    """
    freq = random_freq(midi_num)
    signal = thinkdsp.SawtoothSignal(freq)
    wave = signal.make_wave(duration, framerate=framerate)
    wave.apodize()
    return wave

def make_ensemble(midi_num, duration):
    notes = [make_note(midi_num, duration) for i in range(10)]
    ensemble = sum(notes)
    ensemble.make_audio()
    return ensemble

note = make_note(60, 1.0)
note.make_audio()
note.write('note.wav')

c = make_ensemble(60, 1.0)
c.make_audio()
c.write('c_ensemble')

midi_nums = [64, 62, 60,     64, 62, 60,    60, 60, 60, 60,      62, 62, 62, 62,      64, 62, 60]
durations = [0.5, 0.5, 1.0,  0.5, 0.5, 1.0, 0.25, 0.25, 0.25, 0.25,  0.25, 0.25, 0.25, 0.25,  0.5, 0.5, 1.0]

waves = [make_ensemble(midi_num, duration) for midi_num, duration in zip(midi_nums, durations)]
wave = reduce(thinkdsp.Wave.__or__, waves)
wave.make_audio()

wave.write('cacophony.wav')

spectrum = c.make_spectrum()
spectrum.plot(high=4000)
spectrum.plot()

plt.show()


